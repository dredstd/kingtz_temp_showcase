using System;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Game.QualityGate
{
	public abstract class AssetQualityGate<T> : QualityGate where T : Object
	{
		protected abstract string GetAssetFilter();
		protected abstract void CheckAsset(T asset);

		protected override void Test()
		{
			string[] assetsGuids = AssetDatabase.FindAssets(GetAssetFilter());

			foreach (string assetsGuid in assetsGuids)
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(assetsGuid);
				T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
				if (asset == null)
				{
					Collect(new Exception($"asset at path '{assetPath}' is null"));
				}
				else
				{
					try
					{
						CheckAsset(asset);
					}
					catch (Exception exception)
					{
						Collect(new Error(new Exception($"asset {assetPath} has error {exception.Message}"), asset));
					}
				}
			}
		}
	}
}