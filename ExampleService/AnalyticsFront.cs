using System;
using System.Collections.Generic;
using Core.Service.Reactive;

namespace Core.Service
{
	public readonly struct AnalyticsBuilder
	{
		private readonly int _id;

		public AnalyticsBuilder(int id)
		{
			_id = id;
		}

		[MustUseReturnValue]
		public AnalyticsBuilder Arg(string argName, string value)
		{
			AnalyticsFront.AddArgument(_id, argName, value);
			return this;
		}

		[MustUseReturnValue]
		public AnalyticsBuilder Arg(string argName, int value)
		{
			AnalyticsFront.AddArgument(_id, argName, value);
			return this;
		}

		[MustUseReturnValue]
		public AnalyticsBuilder Arg(string argName, uint value)
		{
			AnalyticsFront.AddArgument(_id, argName, value);
			return this;
		}

		[MustUseReturnValue]
		public AnalyticsBuilder Arg(string argName, float value)
		{
			AnalyticsFront.AddArgument(_id, argName, value);
			return this;
		}

		public void Send(bool immediately = false)
		{
			AnalyticsFront.Complete(_id, immediately);
		}
	}

	public readonly struct AnalyticsEvent
	{
		public readonly string Name;
		public readonly Dictionary<string, object> Arguments;
		public readonly bool Immediately;

		public AnalyticsEvent(string name, Dictionary<string, object> arguments, bool immediately)
		{
			Name = name;
			Arguments = arguments;
			Immediately = immediately;
		}
	}

	public static class AnalyticsFront
	{
		private static string _eventName;
		private static readonly Dictionary<string, object> _args = new Dictionary<string, object>();
		private static int _started;
		private static int _finished;
		public static Event<AnalyticsEvent> OnEvent = new Event<AnalyticsEvent>(EventBlocker.Empty);

#if UNITY_EDITOR
		[UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.SubsystemRegistration)]
		private static void Init()
		{
			OnEvent = new Event<AnalyticsEvent>(EventBlocker.Empty);
			_args.Clear();
		}
#endif

		[MustUseReturnValue]
		public static AnalyticsBuilder Event(string name)
		{
			if (_started != _finished)
			{
				throw new Exception($"Unfinished Event {_eventName}");
			}
			_started++;
			_eventName = name;
			_args.Clear();
			return new AnalyticsBuilder(_started);
		}

		internal static void AddArgument(int id, string argName, object value)
		{
			if (_started != id)
			{
				throw new Exception($"AddArgument Event {id.ToString()} but need to {_started.ToString()}");
			}
			_args.Add(argName, value);
		}

		internal static void Complete(int id, bool immediately)
		{
			if (_started != id)
			{
				throw new Exception($"Complete Event {id.ToString()} but need to {_started.ToString()}");
			}
			OnEvent.Dispatch(new AnalyticsEvent(_eventName, _args, immediately));
			_finished = _started;
		}
	}
}