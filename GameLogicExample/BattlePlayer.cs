﻿using System.Collections.Generic;
using Core.Model;
using Core.Service;

namespace Core.Battle
{
	public class BattlePlayer
	{
		private readonly List<IStrategy> _strategies = new List<IStrategy>()
		{
				new ClearModelRoundStrategy(),
				new StepOrderStrategy(),
				new UpdateSkillsCooldownStrategy(),
				new AvailableUnitsStrategy(),
				new AutoSelectTargetStrategy(),
				new AutoSelectSkillStrategy(),
				new UseSkillStrategy(),
				new CheckVictoryStrategy()
		};

		public void Execute(BattleModel battleModel)
		{
			while (battleModel.BattleStatus != BattleStatus.Finished)
			{
				BLog.Print("BattlePlayer:: Cycle");
				foreach (IStrategy strategy in _strategies)
				{
					strategy.Execute(battleModel);
					if (battleModel.BattleStatus == BattleStatus.Finished)
					{
						return;
					}
					BLog.Print("");
				}
			}
		}
	}
}