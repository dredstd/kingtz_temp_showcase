using System;
using Core.Model;
using UnityEngine;

namespace Game.QualityGate
{
	public class StaticItemQualityGate : QualityGate
	{
		protected override void Test()
		{
			//Example configuration validator

			ItemStatic[] itemStatics = Statics.GetAll<ItemStatic>();
			foreach (ItemStatic itemStatic in itemStatics)
			{
				try
				{
					Asserts.Empty(itemStatic.Name);
					Asserts.Resource<Sprite>(itemStatic.ImagePath);
				}
				catch (Exception exception)
				{
					Collect(new Exception($"Item static id = '{itemStatic.Id}' {exception.Message}"));
				}
			}
		}
	}
}