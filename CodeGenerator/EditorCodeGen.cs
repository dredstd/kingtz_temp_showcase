using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Core.Model;
using Core.Model.LevelRequirements;
using Core.Service;
using UnityEngine;

namespace Game.Installer
{
	public class EditorCodeGen : MonoBehaviour
	{
#if UNITY_EDITOR
		private void OnValidate()
		{
			GenerateCommandRegistrations();
			GenerateHelpPlacements();
			GenerateLevelRequirements();
		}

		private void GenerateHelpPlacements()
		{
			Test.EquipmentTester.LoadStatic();
			var placementStatics = Statics.GetAll<HelpPlacementStatic>();
			StringBuilder sb = new StringBuilder();
			foreach (HelpPlacementStatic placementStatic in placementStatics)
			{
				sb.AppendLine($"		{placementStatic.Name} = {placementStatic.Id},");
			}
			
			WriteToFile($"{Application.dataPath}/Core/Model/HelpProgress/HelpPlacementEnum.cs", sb.ToString());
		}

		private void GenerateLevelRequirements()
		{
			Test.EquipmentTester.LoadStatic();
			LevelRequirementStatic[] levelRequirementsStatic = Statics.GetAll<LevelRequirementStatic>();
			StringBuilder sb = new StringBuilder();
			foreach (LevelRequirementStatic levelRequirementStatic in levelRequirementsStatic)
			{
				sb.AppendLine($"		{levelRequirementStatic.Name} = {levelRequirementStatic.Id.ToString()},");
			}
			
			WriteToFile($"{Application.dataPath}/Core/Model/LevelRequirement/LevelRequirementEnum.cs", sb.ToString());
		}

		private static void GenerateCommandRegistrations()
		{
			Type baseCommandType = typeof(BaseCommand);
			List<Type> commands = new List<Type>();
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (assembly.FullName.Contains("CommandAssembly") ||
				    assembly.FullName.Contains("GameAssembly"))
				{
					foreach (Type type in assembly.GetTypes())
					{
						if (type.IsSubclassOf(baseCommandType) && !type.IsAbstract)
						{
							commands.Add(type);
						}
					}
				}
			}
			commands.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.Ordinal));

			string installerFilePath = $"{Application.dataPath}/Global/Installer/Installer.cs";
			StringBuilder sb = new StringBuilder();
			foreach (Type command in commands)
			{
				sb.AppendLine($"			controller.Bind<{command.Name}>();");
			}
			WriteToFile(installerFilePath, sb.ToString());
		}

		private static void WriteToFile(string path, string data)
		{
			var oldText = File.ReadAllText(path);
			const string codegenStart = "//CODEGEN_START";
			int startCodegenIndex = oldText.IndexOf(codegenStart, StringComparison.Ordinal);
			const string codegenEnd = "//CODEGEN_END";
			int endCodegenIndex = oldText.IndexOf(codegenEnd, StringComparison.Ordinal);
			StringBuilder sb = new StringBuilder(oldText.Length);
			sb.Append(oldText.Substring(0, startCodegenIndex));
			sb.AppendLine(codegenStart);
			sb.Append(data);
			sb.Append(codegenEnd);
			sb.Append(oldText.Substring(endCodegenIndex + codegenEnd.Length));
			string newText = sb.ToString();
			if (newText != oldText)
			{
				File.WriteAllText(path, newText);
			}
		}
#endif
	}
}