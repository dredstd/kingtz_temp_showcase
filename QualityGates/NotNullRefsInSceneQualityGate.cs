using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Game.QualityGate
{
	public class NotNullRefsInSceneQualityGate : QualityGate
	{
		private static readonly string[] AllowedAssemblies =
		{
				"GameAssembly",
				"Assembly-CSharp"
		};

		private readonly Type[] _stopInspectTypes =
		{
				typeof(MonoBehaviour),
				typeof(Component)
		};
		private static readonly Type SerializableType = typeof(SerializeField);
		private static readonly Type CanBeNullType = typeof(CanBeNullAttribute);
		private static readonly Type NotUsedInBuildType = typeof(NotUsedInBuildAttribute);
		private static readonly Type ObjectType = typeof(Object);

		protected override void Test()
		{
			Component[] components = Object.FindObjectsOfType<Component>(true);
			foreach (Component component in components)
			{
				Type componentType = component.GetType();

				if (IsProjectType(componentType))
				{
					InspectFields(component, componentType);
				}
			}
		}

		private void InspectFields(Component component, Type type)
		{
			foreach (Type stopInspectType in _stopInspectTypes)
			{
				if (stopInspectType == type)
				{
					return;
				}
			}
			if (type.BaseType != null)
			{
				InspectFields(component, type.BaseType);
			}

			foreach (FieldInfo serializableField in GetSerializableNotNullFields(type))
			{
				object value = serializableField.GetValue(component);
				if (value == null)
				{
					Collect(new Exception(
							$"{type.Name}: {serializableField.FieldType.Name} is null at {SceneManager.GetActiveScene().name}/{GetObjectScenePath(component)}"));
				}
				else if (ObjectType.IsAssignableFrom(serializableField.FieldType))
				{
					if ((Object)value == null)
					{
						Collect(new Exception(
								$"{type.Name}: {serializableField.FieldType.Name} is null at {SceneManager.GetActiveScene().name}/{GetObjectScenePath(component)}"));
					}
				}
			}
		}

		private static string GetObjectScenePath(Component component)
		{
			Stack<string> hierarchy = new Stack<string>();
			Transform parent = component.transform;
			while (parent != null)
			{
				hierarchy.Push(parent.name);
				parent = parent.parent;
			}
			StringBuilder sb = new StringBuilder();
			while (hierarchy.Count > 0)
			{
				sb.Append(hierarchy.Pop());
				sb.Append("/");
			}
			return sb.ToString();
		}

		private static IEnumerable<FieldInfo> GetSerializableNotNullFields(Type type)
		{
			if (type.IsDefined(NotUsedInBuildType))
			{
				yield break;
			}
			foreach (FieldInfo fieldInfo in GetSerializableFields(type))
			{
				if (!fieldInfo.IsDefined(CanBeNullType))
				{
					yield return fieldInfo;
				}
			}
		}

		public static IEnumerable<FieldInfo> GetSerializableFields(Type type)
		{
			FieldInfo[] fieldInfos =
					type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			foreach (FieldInfo fieldInfo in fieldInfos)
			{
				if (fieldInfo.IsPublic || fieldInfo.IsDefined(SerializableType))
				{
					if (fieldInfo.FieldType.IsClass)
					{
						yield return fieldInfo;
					}
				}
			}
		}

		public static bool IsProjectType(Type componentType)
		{
			foreach (string allowedAssembly in AllowedAssemblies)
			{
				if (componentType.Assembly.FullName.StartsWith(allowedAssembly))
				{
					return true;
				}
			}
			return false;
		}
	}
}