using System;
using UnityEditor;
using UnityEngine;

namespace Game.QualityGate
{
	public class CleanCoreQualityGate : QualityGate
	{
		private static readonly string[] Disallowed =
		{
				"UnityEngine",
				"Game.",
				"Game;"
		};
		private static readonly string[] IgnorePatterns =
		{
				"#if UNITY_EDITOR",
				"#if UNITY_2020"
		};

		protected override void Test()
		{
			string[] fileGuids = AssetDatabase.FindAssets("t:Script", new[] {"Assets/Core"});
			foreach (string fileGuid in fileGuids)
			{
				string assetPath = AssetDatabase.GUIDToAssetPath(fileGuid);
				TestFile(assetPath);
			}
		}

		private void TestFile(string assetPath)
		{
			string fileContent = AssetDatabase.LoadAssetAtPath<TextAsset>(assetPath).text;

			foreach (string ignorePattern in IgnorePatterns)
			{
				if (fileContent.Contains(ignorePattern))
				{
					return;
				}
			}
			foreach (string test in Disallowed)
			{
				if (fileContent.Contains(test))
				{
					Collect(new Exception($"{assetPath} Contains {test}"));
				}
			}
		}
	}
}