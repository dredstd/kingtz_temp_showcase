using System;
using System.Collections.Generic;
using Core.Service.Reactive;
using UnityEngine;

namespace Game.View
{
	public class ViewBehaviour : MonoBehaviour
	{
		private bool _wasActive = false;
		private List<Listener> _listeners;

		protected IFieldListener Subscribe<T>(Field<T> field, Action<T> action)
		{
			if (_listeners == null)
			{
				_listeners = new List<Listener>();
			}
			else
			{
				foreach (Listener listener in _listeners)
				{
					if (listener.IsListenTo(field))
					{
						throw new Exception($"Trying to subscribe twice for one Field<{typeof(T).Name}>");
					}
				}
			}

			FieldListener<T> fieldListener = new FieldListener<T>(field, arg =>
			{
				if (IsAlive())
				{
					action(arg);
				}
				else
				{
					OnDestroy();
				}
			});
			_listeners.Add(fieldListener);
			return fieldListener;
		}

		protected void Subscribe<T>(Event<T> eventField, Action<T> action)
		{
			if (_listeners == null)
			{
				_listeners = new List<Listener>();
			}

			_listeners.Add(new EventListener<T>(eventField, arg =>
			{
				if (IsAlive())
				{
					action(arg);
				}
				else
				{
					OnDestroy();
				}
			}));
		}

		private void OnEnable()
		{
			_wasActive = true;
			AfterEnable();
		}

		private bool IsAlive()
		{
			if (_wasActive)
			{
				return true;
			}
			if (this != null && gameObject != null)
			{
				return true;
			}
			return false;
		}

		protected void UpdateSubscriptions()
		{
			if (_listeners != null)
			{
				foreach (Listener listener in _listeners)
				{
					if (listener is IFieldListener fieldListener)
					{
						fieldListener.Update();
					}
				}
			}
		}

		public void OnDestroy()
		{
			ClearSubscriptions();
			AfterDestroy();
		}

		protected void ClearSubscriptions()
		{
			if (_listeners != null)
			{
				foreach (Listener fieldListener in _listeners)
				{
					fieldListener.Dispose();
				}
				_listeners.Clear();
			}
		}

		protected void ClearSubscriptions<T>(Field<T> field)
		{
			ClearSubscriptions((object)field);
		}

		protected void ClearSubscriptions<T>(Event<T> eventField)
		{
			ClearSubscriptions((object)eventField);
		}

		private void ClearSubscriptions(object field)
		{
			if (_listeners != null)
			{
				for (int i = _listeners.Count - 1; i >= 0; i--)
				{
					Listener fieldListener = _listeners[i];
					if (fieldListener.IsListenTo(field))
					{
						fieldListener.Dispose();
						_listeners.RemoveAt(i);
					}
				}
			}
		}

		protected virtual void AfterDestroy()
		{
		}

		protected virtual void AfterEnable()
		{
		}
	}
}