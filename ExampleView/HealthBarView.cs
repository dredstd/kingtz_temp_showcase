﻿using Core.Model;
using Game.View;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
	public class HealthBarView : ViewBehaviour
	{
		[SerializeField]
		private Slider _healthBarSlider;
		[SerializeField]
		public Text _healthPointsText;
		[SerializeField]
		private AnimationCurve _animationCurve = AnimationCurve.Linear(0, 1, 0, 1);
		[SerializeField]
		private float _slideDuration = 1.0f;

		private int _startHealthPoints;
		private Unit _unit;

		public void Init(Unit unit)
		{
			_unit = unit;
			_startHealthPoints = _unit.HealthPoints;
			_healthBarSlider.value = _unit.Health.Value / (float)_startHealthPoints;
			_healthPointsText.text =  _unit.Health.Value.ToString();
			Subscribe(_unit.Health, OnHealthChanged);
		}

		private void OnHealthChanged(int health)
		{
			UpdateHealthBar(_unit.Health.PreviousValue, health);
		}

		private void UpdateHealthBar(int fromHealthPoints, int toHealthPoints)
		{
			float normalizedHealth = (float) toHealthPoints / _unit.HealthPoints;
			_healthBarSlider.SlideSmoothly(normalizedHealth, _slideDuration, _animationCurve);
			_healthPointsText.ChangeIntTextSmoothly(fromHealthPoints, toHealthPoints, _slideDuration,
				_animationCurve);
		}
	}
}