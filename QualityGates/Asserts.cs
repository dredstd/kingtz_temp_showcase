using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Core.Model;
using Core.Model.Dynamic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Game.QualityGate
{
	public static class Asserts
	{
		// There is more, i cut it
		public static void Range(int value, int min, int maxExc,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (value < min || value >= maxExc)
			{
				throw new Exception(
						$"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' value '{value.ToString()}' out of range '[{min.ToString()},{maxExc.ToString()} )'");
			}
		}

		public static TComponent Resource<TComponent>(string value,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0) where TComponent : Object
		{
			Type componentType = typeof(TComponent);
			Object component = Resources.Load(value, componentType);
			if (component == null)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' asset type '{componentType.Name}' not found {value}");
			}
			return (TComponent)component;
		}

		public static void EmptyItem(ItemPair itemPair,
				[CallerFilePath] string sourceFilePath = "",
				[CallerLineNumber] int sourceLineNumber = 0)
		{
			if (itemPair.Count == 0)
			{
				throw new Exception($"field '{ArgHelper.Get(sourceFilePath, sourceLineNumber)}' is zero count item '{itemPair.Id}'");
			}
			var itemStatic = itemPair.ItemStatic;
		}
	}
}